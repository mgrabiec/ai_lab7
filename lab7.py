import matplotlib.pyplot as plt
import matplotlib.image as mpimg
img = mpimg.imread('activationFunction.png')
imgplot = plt.imshow(img)

import numpy as np
import pandas as pd
import itertools
from tabulate import tabulate
import operator


# activation function
def activate(data:float)->float:
    return 0 if data<0.7 else 1

# compute layer function 
def layerCompute(inputs:list,weights:list,activation_function = None):
	inputs = np.array([1]  +inputs).transpose()
	weights = np.array(weights).transpose()
	output = inputs.dot(weights)
	if type(output) is not np.ndarray:
		output = [output]
	else:
		output.T
	if activation_function:
		output = [(activate(x),x) for x in output]
	return output

# print layer function
def layerPrint(inputs, weights, bias,result):
	to_print = []
	for idx,input in enumerate(inputs):
		act_output,raw_output = result[idx]
		to_print.append([input,weights,bias,raw_output,act_output])
	print(tabulate(to_print,headers=["IN","OUT","BIAS","RAW","ACTIVATION"]))

print('\n\n\nXNOR')
bias = [[0.7],[-1.3]]
weights = [[-1,-1],[1,1]]
inputs = [list(p) for p in itertools.product([1,0],repeat=2)]
result = []
for input in inputs:
	results = layerCompute(input,np.concatenate((bias,weights),axis=1),activation_function=activate)
	result.append(([result[0]for result in results],[result[1]for result in results]))
layerPrint(inputs,weights,bias,result)


print('\n\n\n')
bias = [-0.3]
weights = [1,1]
inputs = [x[0] for x in result]
result = []
for input in inputs:
	results = layerCompute(input,bias+weights,activation_function=activate)[0]
	result.append(results)
layerPrint(inputs,weights,bias,result)


print('\n\n\nAND:')
bias = -1.3
weights = [1,1]
inputs = [list(p) for p in itertools.product([0,1],repeat=2)]
result = []
for idx,input in enumerate(inputs):
	output,raw_output = layerCompute(input,[bias]+weights,activation_function=activate)[0]
	result.append((output,raw_output))
layerPrint(inputs,weights,bias,result)

print('\n\n\nXNOR')
bias = [[-0.3],[1.7]]
weights = [[1,1],[-1,-1]]
inputs = [list(p) for p in itertools.product([1,0],repeat=2)]
result = []
for input in inputs:
	results = layerCompute(input,np.concatenate((bias,weights),axis=1),activation_function=activate)
	result.append(([result[0]for result in results],[result[1]for result in results]))
layerPrint(inputs,weights,bias,result)


print('\n\n\n')
bias = [-1.3]
weights = [1,1]
inputs = [x[0] for x in result]
result = []
for input in inputs:
	results = layerCompute(input,bias+weights,activation_function=activate)[0]
	result.append(results)
layerPrint(inputs,weights,bias,result)


print('\n\n\nOR:')
bias = -0.3
weights = [1,1]
inputs = [list(p) for p in itertools.product([0,1],repeat=2)]
result = []
for input in inputs:
	output,raw_output = layerCompute(input,[bias]+weights,activation_function=activate)[0]
	result.append((output,raw_output))
layerPrint(inputs,weights,bias,result)


print('\n\n\nNOT')
bias =0.7
weights = [-2.3]
inputs = [list(p) for p in itertools.product([1,0],repeat=1)]
result = []
for input in inputs:
	output,raw_output = layerCompute(input,[bias]+weights,activation_function=activate)[0]
	result.append((output,raw_output))
layerPrint(inputs,weights,bias,result)


plt.show()